package umb.teknik.pariwisata.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import umb.teknik.pariwisata.model.TempatWisata;

@Database(entities = {TempatWisata.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

	public abstract DaoTempatWisata daoTempatWisata();
}
