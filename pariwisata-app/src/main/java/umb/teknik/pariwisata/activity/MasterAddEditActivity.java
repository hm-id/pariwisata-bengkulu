package umb.teknik.pariwisata.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Objects;

import umb.teknik.pariwisata.R;
import umb.teknik.pariwisata.dao.DaoTempatWisata;
import umb.teknik.pariwisata.helper.AppHelper;
import umb.teknik.pariwisata.model.TempatWisata;

public class MasterAddEditActivity extends AppCompatActivity implements IPickResult {

	private static final int PLACE_PICKER_REQUEST = 11;
	private DaoTempatWisata mDao;
	private Uri mImage;
	private LatLng mWisataLatLng;
	private ImageView mFoto;
	private TextInputEditText mLokasi, mNama, mAlamat, mJam, mHari, mKet;
	private boolean mIsUpdate = false;
	private long mSelectedId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_master_add_edit);

		mFoto = findViewById(R.id.master_image);
		mLokasi = findViewById(R.id.master_location);
		mNama = findViewById(R.id.master_name);
		mAlamat = findViewById(R.id.master_address);
		mJam = findViewById(R.id.master_clock);
		mHari = findViewById(R.id.master_day);
		mKet = findViewById(R.id.master_detail);

		mDao = AppHelper.database(this).daoTempatWisata();
		mSelectedId = getIntent().getLongExtra(MasterActivity.SELECTED_ID, -1L);
		if (mSelectedId != -1L) {
			populateToView();
		}
	}

	private void populateToView() {
		TempatWisata wisata = mDao.get(mSelectedId);
		setTitle(String.format("Edit (%s)", wisata.nama));
		mNama.setText(wisata.nama);
		mAlamat.setText(wisata.alamat);
		mJam.setText(wisata.jamTersedia);
		mHari.setText(wisata.hariTersedia);
		mKet.setText(wisata.keterangan);
		String location = String.format("Lat:%s\nLng:%s", wisata.latitude, wisata.longitude);
		mLokasi.setText(location);
		mWisataLatLng = new LatLng(wisata.latitude, wisata.longitude);
		Glide.with(this).load(AppHelper.imageFile(this, wisata.nama)).into(mFoto);
		mIsUpdate = true;
	}

	public void selectImage(View view) {
		PickImageDialog.build(new PickSetup().setSystemDialog(true)).show(this);
	}

	public void selectLocation(View view) {
		try {
			PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
			startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
		} catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
			e.printStackTrace();
		}
	}

	private String getValue(EditText e) {
		if (e.getText() != null) {
			return e.getText().toString();
		}
		return "";
	}

	private TempatWisata getFromView() {
		String nama = getValue(mNama);
		String alamat = getValue(mAlamat);
		String jam = getValue(mJam);
		String hari = getValue(mHari);
		String ket = getValue(mKet);
		if (nama.isEmpty()) {
			mNama.setError(getString(R.string.cant_null));
			return null;
		} else if (alamat.isEmpty()) {
			mAlamat.setError(getString(R.string.cant_null));
			return null;
		} else if (jam.isEmpty()) {
			mJam.setError(getString(R.string.cant_null));
			return null;
		} else if (hari.isEmpty()) {
			mHari.setError(getString(R.string.cant_null));
			return null;
		} else if (ket.isEmpty()) {
			mKet.setError(getString(R.string.cant_null));
			return null;
		} else if (getValue(mLokasi).isEmpty()) {
			mLokasi.setError(getString(R.string.cant_null));
			return null;
		}
		TempatWisata t = new TempatWisata();
		t.nama = nama;
		t.alamat = alamat;
		t.jamTersedia = jam;
		t.hariTersedia = hari;
		t.keterangan = ket;
		t.latitude = mWisataLatLng.latitude;
		t.longitude = mWisataLatLng.longitude;
		return t;
	}

	public void onSave(View view) {
		TempatWisata wisata = getFromView();
		if (wisata == null) {
			return;
		}
		try {
			if (!mIsUpdate) {
				if (mImage != null) {
					if (saveImage(wisata.nama) && mDao.create(wisata) > 0) {
						info("Tersimpan");
						finish();
					} else {
						info("Gagal menyimpan!");
					}
				} else {
					info("Harap pilih gambar!");
				}
			} else {
				wisata.id = mSelectedId;
				boolean upd = mImage == null || saveImage(wisata.nama);
				if (upd && mDao.update(wisata) > 0) {
					info("Update berhasil");
					finish();
				} else {
					info("Update gagal!");
				}
			}
		} catch (Exception e) {
			info("Error! Nama mungkin sudah digunakan. Nama tidak boleh sama. "
					+ e.getLocalizedMessage());
		}
	}

	private void info(String msg) {
		AppHelper.showMessage(this, 0, msg);
	}

	public void onCancel(View view) {
		onBackPressed();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == PLACE_PICKER_REQUEST) {
			if (resultCode == RESULT_OK) {
				Place place = PlacePicker.getPlace(this, data);
				mWisataLatLng = place.getLatLng();
				String location = String.format("Lat:%s\nLng:%s",
						place.getLatLng().latitude, place.getLatLng().longitude);
				mLokasi.setText(location);
				mAlamat.setText(place.getAddress());
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPickResult(PickResult r) {
		if (r.getError() == null) {
			mImage = r.getUri();
			mFoto.setImageBitmap(r.getBitmap());
		}
	}

	private boolean saveImage(String nama) {
		File to = AppHelper.imageFile(this, nama);
		final int chunkSize = 1024;
		byte[] imageData = new byte[chunkSize];
		try (InputStream in = getContentResolver().openInputStream(mImage);
				 OutputStream out = new FileOutputStream(to)) {
			int bytesRead;
			while ((bytesRead = Objects.requireNonNull(in).read(imageData)) > 0) {
				out.write(Arrays.copyOfRange(imageData, 0, Math.max(0, bytesRead)));
			}
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
}