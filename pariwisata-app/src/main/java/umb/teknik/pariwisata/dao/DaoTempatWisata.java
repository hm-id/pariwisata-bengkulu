package umb.teknik.pariwisata.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import umb.teknik.pariwisata.model.TempatWisata;

@Dao
public interface DaoTempatWisata {

	@Query("SELECT * FROM tempatwisata WHERE id=:id")
	TempatWisata get(long id);

	@Query("SELECT * FROM tempatwisata WHERE nama LIKE :key")
	List<TempatWisata> filter(String key);

	@Query("SELECT * FROM tempatwisata")
	List<TempatWisata> getAll();

	@Insert
	long create(TempatWisata tempatWisata);

	@Update
	int update(TempatWisata tempatWisata);

	@Delete
	int delete(TempatWisata tempatWisata);
}
