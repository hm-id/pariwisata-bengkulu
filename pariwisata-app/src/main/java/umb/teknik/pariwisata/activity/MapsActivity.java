package umb.teknik.pariwisata.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;
import umb.teknik.pariwisata.R;
import umb.teknik.pariwisata.dao.DaoTempatWisata;
import umb.teknik.pariwisata.helper.AppHelper;
import umb.teknik.pariwisata.helper.Dijkstra;
import umb.teknik.pariwisata.model.TempatWisata;


@SuppressWarnings("ALL")
public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, Callback<DirectionsResponse> {

	private static final String TAG = MapsActivity.class.getName();
	private static final int SEARCH_RESULT = 113;
	private boolean mDialogSetting = false;
	private int mTestCount = 0;
	private DaoTempatWisata mDao;

	private TempatWisata mWisata;
	private MapboxMap mMapboxMap;
	private MapView mMapView;
	private Style mStyle;
	private LatLng mUser;
	private NavigationMapRoute mNavigationMapRoute;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Mapbox.getInstance(this, getString(R.string.mapbox_token));
		setContentView(R.layout.activity_maps);
		mDao = AppHelper.database(this).daoTempatWisata();
		mMapView = findViewById(R.id.mapView);
		mMapView.onCreate(savedInstanceState);
		mMapView.getMapAsync(this);
		setLoading(true, "Mendapatkan lokasi anda. Mohon tunggu...");
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		getMenuInflater().inflate(R.menu.menu_maps, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.item_map_search) {
			startActivityForResult(new Intent(this, SearchActivity.class), SEARCH_RESULT);
		}
		return super.onOptionsItemSelected(item);
	}

	private void setLoading(boolean b, String message) {
		((TextView) findViewById(R.id.maps_pb_message)).setText(message);
		findViewById(R.id.maps_pb_container).setVisibility(b ? View.VISIBLE : View.GONE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SEARCH_RESULT && resultCode == RESULT_OK) {
			long id = data.getLongExtra(SearchActivity.TEMPAT_WISATA_ID, -1L);
			mWisata = mDao.get(id);
			process();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mMapView.onResume();
		if (!mDialogSetting) {
			if (!AppHelper.isGpsEnabled(this)) AppHelper.gpsSetting(this).show();
		}
		mDialogSetting = true;
	}

	@Override
	protected void onStart() {
		super.onStart();
		mMapView.onStart();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mMapView.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mMapView.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mMapView.onSaveInstanceState(outState);
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mMapView.onLowMemory();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mMapView.onDestroy();
	}

	@Override
	public void onMapReady(@NonNull MapboxMap mapboxMap) {
		this.mMapboxMap = mapboxMap;
		mMapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
			mStyle = style;
			mNavigationMapRoute = new NavigationMapRoute(null, mMapView, mMapboxMap);
			processUserLocation();
		});
	}

	@SuppressLint("MissingPermission")
	private void processUserLocation() {
		LocationComponentOptions options = LocationComponentOptions.builder(this)
				.trackingGesturesManagement(true)
				.accuracyColor(ContextCompat.getColor(this, R.color.colorPrimary))
				.build();
		LocationComponent location = mMapboxMap.getLocationComponent();
		location.activateLocationComponent(this, mStyle, options);
		location.setLocationComponentEnabled(true);
		Location knownLocation = location.getLastKnownLocation();
		if (knownLocation != null) {
			mUser = new LatLng(knownLocation.getLatitude(), knownLocation.getLongitude());
		} else {
			AppHelper.showMessage(this, 1, "Tidak dapat menemukan lokasi anda!");
		}
		setLoading(false, null);
	}

	private void showDetail(TempatWisata wisata) {
		Intent intent = new Intent(MapsActivity.this, InfoActivity.class);
		intent.putExtra(InfoActivity.ID_WISATA, wisata.id);
		startActivity(intent);
	}

	private void getRoutes() {
		Point origin = Point.fromLngLat(mUser.getLongitude(), mUser.getLatitude());
		Point destination = Point.fromLngLat(mWisata.longitude, mWisata.latitude);
		NavigationRoute.builder(this).accessToken(getString(R.string.mapbox_token))
				.origin(origin)
				.destination(destination)
				.alternatives(true)
				.profile(DirectionsCriteria.PROFILE_WALKING)
				.build()
				.getRoute(this);
	}

	@Override
	public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
		if (response.body() == null) {
			String invalidToken = getString(R.string.invalid_token);
			Timber.e(TAG, invalidToken);
			AppHelper.showMessage(this, 0, invalidToken);
			if (mTestCount < 3) process();
		} else if (response.body().routes().isEmpty()) {
			String noRoutes = getString(R.string.no_routes);
			Timber.e(TAG, noRoutes);
			AppHelper.showMessage(this, 0, noRoutes);
			if (mTestCount < 3) process();
		} else {
			List<DirectionsRoute> routes = response.body().routes();
			drawRoutes(routes);
		}
		setLoading(false, null);
	}

	@Override
	public void onFailure(Call<DirectionsResponse> call, Throwable t) {
		AppHelper.showMessage(this, 0, "Proses pencarian rute gagal!");
	}

	private void drawRoutes(List<DirectionsRoute> routes) {
		mMapboxMap.clear();
		StringBuilder sb = new StringBuilder();
		mNavigationMapRoute.addRoutes(routes);
		sb.append("Rute terdekat ditemukan!\n");
		int i = 1;
		for (DirectionsRoute dr : routes) {
			sb.append("Rute ").append(i).append(": Jarak ").append(dr.distance()).append(" meter\n");
			i++;
		}
		DirectionsRoute min = Collections.min(routes, (a, b) ->
				Objects.requireNonNull(a.distance()).compareTo(Objects.requireNonNull(b.distance())));
		String routeMin = "Jarak rute terdekat: " + min.distance() + " meter";
		sb.append('\n').append(routeMin);
		AppHelper.showMessage(this, 0, routeMin);
		AppHelper.dialog(this).setTitle("Hasil").setMessage(sb.toString())
				.setCancelable(false).setPositiveButton(R.string.ok, null).create().show();

		// Add marker and animateCamera
		mMapboxMap.addMarker(new MarkerOptions().position(new LatLng(mUser.getLatitude(), mUser.getLongitude())));
		Marker markWisata = mMapboxMap.addMarker(new MarkerOptions().position(mWisata.getLatLng()).title(mWisata.nama));
		mMapboxMap.setOnInfoWindowClickListener((m) -> {
			if (m.equals(markWisata)) showDetail(mWisata);
			return false;
		});
		mMapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mWisata.getLatLng(), 14), 2000);
	}

	private void process() {
		setLoading(true, "Mohon tunggu...");
		Dijkstra dijkstra = new Dijkstra();
		Dijkstra.Graph graph = new Dijkstra.Graph();
		Dijkstra.Node node = new Dijkstra.Node("my_location");
		node.setDistance(0);
		graph.addNode(node);
		Dijkstra.Graph path = dijkstra.calculateShortestPathFromSource(graph, node);
		Timber.w(TAG, path);
		getRoutes();
		mTestCount++;
	}
}