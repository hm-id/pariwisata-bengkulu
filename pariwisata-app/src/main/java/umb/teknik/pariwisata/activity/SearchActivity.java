package umb.teknik.pariwisata.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import umb.teknik.pariwisata.R;
import umb.teknik.pariwisata.dao.DaoTempatWisata;
import umb.teknik.pariwisata.helper.AppHelper;
import umb.teknik.pariwisata.helper.MasterListAdapter;
import umb.teknik.pariwisata.model.TempatWisata;

public class SearchActivity extends AppCompatActivity
		implements SearchView.OnQueryTextListener, AdapterView.OnItemClickListener {

	public static final String TEMPAT_WISATA_ID = "twid";
	private DaoTempatWisata mDao;
	private MasterListAdapter mAdapter;
	private List<TempatWisata> mData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		ListView list = findViewById(R.id.search_list);

		mDao = AppHelper.database(this).daoTempatWisata();

		mData = new ArrayList<>();
		mAdapter = new MasterListAdapter(this, mData);
		list.setAdapter(mAdapter);
		list.setEmptyView(findViewById(R.id.search_list_empty));
		list.setOnItemClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		populateData(mDao.getAll());
	}

	private void populateData(List<TempatWisata> data) {
		mData.clear();
		mData.addAll(data);
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		getMenuInflater().inflate(R.menu.menu_search, menu);
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.item_search).getActionView();
		if (searchManager != null) {
			searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		}
		searchView.setOnQueryTextListener(this);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		populateData(mDao.filter('%' + query + '%'));
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		if (newText.isEmpty()) {
			populateData(mDao.getAll());
		} else {
			populateData(mDao.filter('%' + newText + '%'));
		}
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		TempatWisata wisata = (TempatWisata) parent.getAdapter().getItem(position);
		Intent intent = new Intent();
		intent.putExtra(TEMPAT_WISATA_ID, wisata.id);
		setResult(Activity.RESULT_OK, intent);
		finish();
	}
}
