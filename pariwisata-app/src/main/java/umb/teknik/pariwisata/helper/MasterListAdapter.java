package umb.teknik.pariwisata.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import umb.teknik.pariwisata.model.TempatWisata;

public class MasterListAdapter extends BaseAdapter {

	private Context context;
	private List<TempatWisata> data;

	public MasterListAdapter(Context context, List<TempatWisata> data) {
		this.context = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public TempatWisata getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return getItem(position).id;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(context)
					.inflate(android.R.layout.simple_list_item_2, parent, false);
		}
		TextView name = convertView.findViewById(android.R.id.text1);
		TextView address = convertView.findViewById(android.R.id.text2);
		address.setSingleLine();
		TempatWisata item = getItem(position);
		name.setText(item.nama);
		address.setText(item.alamat);
		return convertView;
	}
}
