package umb.teknik.pariwisata.helper;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;

import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import java.io.File;
import java.util.UUID;

import umb.teknik.pariwisata.R;
import umb.teknik.pariwisata.dao.AppDatabase;

public class AppHelper {

	public static AlertDialog.Builder dialog(Context context) {
		return new AlertDialog.Builder(context);
	}

	public static void showDialog(Context context, int title, int message) {
		dialog(context).setTitle(title).setMessage(message)
				.setPositiveButton("OK", null).show();
	}

	public static boolean checkPermission(Context context) {
		int granted = PackageManager.PERMISSION_GRANTED;
		int fine = ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
		int coarse = ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
		return fine == granted && coarse == granted;
	}

	public static AppDatabase database(Context context) {
		return Room.databaseBuilder(context, AppDatabase.class, "database.db")
				.allowMainThreadQueries().build();
	}

	private static String md5(String plainText) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes());
			byte data[] = md.digest();
			StringBuilder sb = new StringBuilder();
			for (byte aData : data) {
				sb.append(Integer.toString((aData & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException ex) {
			System.err.println(ex.getMessage());
			return null;
		}
	}

	public static File imageFile(Context context, String nama) {
		return new File(context.getExternalFilesDir("images"), md5(nama) + ".dat");
	}

	public static void showMessage(Activity activity, int mode, String message) {
		int duration = mode == 0 ? Snackbar.LENGTH_INDEFINITE : Snackbar.LENGTH_LONG;
		final Snackbar snack = Snackbar.make(activity.findViewById(android.R.id.content), message, duration);
		if (mode == 0) {
			snack.setAction("OK", v -> snack.dismiss());
		}
		snack.show();
	}

	public static AlertDialog.Builder gpsSetting(final Context context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(false).setTitle("GPS Tidak Aktif");
		builder.setMessage(R.string.gps_disable_info);
		builder.setPositiveButton("GPS Setting", (dialog, which) -> context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))).setNegativeButton("Abaikan", null);
		return builder;
	}

	public static boolean isGpsEnabled(Context context) {
		LocationManager service = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		return service != null && service.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}
}