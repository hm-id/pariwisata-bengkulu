package umb.teknik.pariwisata.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import umb.teknik.pariwisata.R;
import umb.teknik.pariwisata.helper.AppHelper;

public class MainActivity extends AppCompatActivity {

	private boolean mExit = false;
	private String[] mPermissions = {
			Manifest.permission.ACCESS_FINE_LOCATION,
			Manifest.permission.ACCESS_COARSE_LOCATION
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		DialogInterface.OnClickListener dialogPermission = (dialog, which) -> {
			if (isAndroidM()) {
				requestPermissions(mPermissions, 102);
			}
		};

		if (isAndroidM()) {
			if (!AppHelper.checkPermission(this)) {
				AppHelper.dialog(this)
						.setCancelable(false)
						.setMessage(R.string.need_permission)
						.setPositiveButton("OK", dialogPermission).show();
			}
		}

	}

	private boolean isAndroidM() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
	}

	@Override
	public void onBackPressed() {
		if (mExit) super.onBackPressed();
		Toast.makeText(this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_LONG).show();
		mExit = true;
		new Handler().postDelayed(() -> mExit = false, 2000);
	}

	public void onTour(View view) {
		startActivity(new Intent(this, MapsActivity.class));
	}

	public void onMaster(View view) {
		startActivity(new Intent(this, MasterActivity.class));
	}

	public void onAbout(View view) {
		AppHelper.showDialog(this, R.string.about, R.string.about_info);
	}
}
