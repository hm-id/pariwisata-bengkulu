package umb.teknik.pariwisata.helper;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Dijkstra {

	public Graph calculateShortestPathFromSource(Graph graph, Node source) {
		source.setDistance(0);
		Set<Node> settledNodes = new HashSet<>();
		Set<Node> unsettledNodes = new HashSet<>();
		unsettledNodes.add(source);

		while (unsettledNodes.size() != 0) {
			Node currentNode = getLowestDistanceNode(unsettledNodes);
			unsettledNodes.remove(currentNode);
			for (Map.Entry<Node, Integer> adjacencyPair :
					currentNode.getAdjacentNodes().entrySet()) {
				Node adjacentNode = adjacencyPair.getKey();
				Integer edgeWeight = adjacencyPair.getValue();
				if (!settledNodes.contains(adjacentNode)) {
					calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
					unsettledNodes.add(adjacentNode);
				}
			}
			settledNodes.add(currentNode);
		}
		return graph;
	}

	private Node getLowestDistanceNode(Set<Node> unsettledNodes) {
		Node lowestDistanceNode = null;
		int lowestDistance = Integer.MAX_VALUE;
		for (Node node : unsettledNodes) {
			int nodeDistance = node.getDistance();
			if (nodeDistance < lowestDistance) {
				lowestDistance = nodeDistance;
				lowestDistanceNode = node;
			}
		}
		return lowestDistanceNode;
	}

	private void calculateMinimumDistance(Node evaluationNode,
																				Integer edgeWeigh, Node sourceNode) {
		Integer sourceDistance = sourceNode.getDistance();
		if (sourceDistance + edgeWeigh < evaluationNode.getDistance()) {
			evaluationNode.setDistance(sourceDistance + edgeWeigh);
			LinkedList<Node> shortestPath = new LinkedList<>(sourceNode.getShortestPath());
			shortestPath.add(sourceNode);
			evaluationNode.setShortestPath(shortestPath);
		}
	}

	public static class Node {

		private String name;
		private List<Node> shortestPath = new LinkedList<>();
		private Integer distance = Integer.MAX_VALUE;
		Map<Node, Integer> adjacentNodes = new HashMap<>();

		public Node(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		List<Node> getShortestPath() {
			return shortestPath;
		}

		void setShortestPath(List<Node> shortestPath) {
			this.shortestPath = shortestPath;
		}

		Integer getDistance() {
			return distance;
		}

		public void setDistance(Integer distance) {
			this.distance = distance;
		}

		Map<Node, Integer> getAdjacentNodes() {
			return adjacentNodes;
		}
	}

	public static class Graph {

		@SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
		private Set<Node> nodes = new HashSet<>();

		public void addNode(Node nodeA) {
			nodes.add(nodeA);
		}

		@Override
		public String toString() {
			String name = nodes.isEmpty() ? "Target" : "" + nodes.size();
			return "Graph{" +
					"nodes=" + name +
					'}';
		}
	}
}
