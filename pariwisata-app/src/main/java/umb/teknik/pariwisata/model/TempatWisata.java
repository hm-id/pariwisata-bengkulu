package umb.teknik.pariwisata.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.mapbox.mapboxsdk.geometry.LatLng;

@Entity(indices = {@Index(value = {"nama"}, unique = true)})
public class TempatWisata {

	@PrimaryKey(autoGenerate = true)
	public long id;
	public String nama;
	public String alamat;
	public double latitude;
	public double longitude;
	public String jamTersedia;
	public String hariTersedia;
	public String keterangan;

	@Ignore
	public LatLng getLatLng() {
		return new LatLng(this.latitude, this.longitude);
	}
}