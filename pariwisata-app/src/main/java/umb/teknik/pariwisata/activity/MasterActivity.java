package umb.teknik.pariwisata.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.obsez.android.lib.filechooser.ChooserDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import umb.teknik.pariwisata.R;
import umb.teknik.pariwisata.dao.DaoTempatWisata;
import umb.teknik.pariwisata.helper.AppHelper;
import umb.teknik.pariwisata.helper.MasterListAdapter;
import umb.teknik.pariwisata.model.TempatWisata;

public class MasterActivity extends AppCompatActivity {

	public static final String SELECTED_ID = "tp_id";
	private DaoTempatWisata mDao;
	private List<TempatWisata> mData;
	private MasterListAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_master);
		ListView listView = findViewById(R.id.master_list);

		mDao = AppHelper.database(this).daoTempatWisata();

		mData = new ArrayList<>();
		mAdapter = new MasterListAdapter(this, mData);
		listView.setAdapter(mAdapter);
		listView.setEmptyView(findViewById(R.id.master_list_empty));

		registerForContextMenu(listView);
	}

	@Override
	protected void onResume() {
		super.onResume();
		populateData();
	}

	private void populateData() {
		mData.clear();
		mData.addAll(mDao.getAll());
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_master, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO: Add menu import and export database
		if (item.getItemId() == R.id.item_add_master) {
			startActivity(new Intent(this, MasterAddEditActivity.class));
		} else if (item.getItemId() == R.id.item_export_master) {
			exportData();
		} else if (item.getItemId() == R.id.item_import_master) {
			AppHelper.dialog(this)
					.setTitle(R.string.warning)
					.setMessage(R.string.import_warning)
					.setPositiveButton("Ya", (dialog, which) -> importData())
					.setNegativeButton("Tidak", null)
					.show();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.master_list) {
			menu.setHeaderTitle("Pilih Aksi");
			menu.add(Menu.NONE, 0, 0, "Edit Data");
			menu.add(Menu.NONE, 1, 1, "Hapus Data");
		}
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		final TempatWisata wisata = mAdapter.getItem(info.position);
		if (item.getItemId() == 0) {
			Intent intent = new Intent(this, MasterAddEditActivity.class);
			intent.putExtra(SELECTED_ID, wisata.id);
			startActivity(intent);
		} else if (item.getItemId() == 1) {
			AppHelper.dialog(this)
					.setMessage("\"" + wisata.nama + "\" akan dihapus?")
					.setPositiveButton("Hapus", (dialog, which) -> {
						if (mDao.delete(wisata) > 0) {
							deleteImage(wisata.nama);
							populateData();
						} else {
							AppHelper.showMessage(MasterActivity.this, 1,
									"Gagal menghapus data!");
						}
					})
					.setNegativeButton("Batal", null).show();
		}
		return super.onContextItemSelected(item);
	}

	private void deleteImage(String nama) {
		File img = AppHelper.imageFile(this, nama);
		boolean del = img.exists() && img.delete();
	}

	private void exportData() {
		File dataDir = Environment.getDataDirectory();
		File dbFile = new File(dataDir, "data/" + getPackageName()
				+ "/databases/database.db");

		File sdDir = Environment.getExternalStorageDirectory();
		File exDbFile = new File(sdDir, "bengkulu_wisata.bak");

		String msg;
		try (FileChannel src = new FileInputStream(dbFile).getChannel();
				 FileChannel dst = new FileOutputStream(exDbFile).getChannel()) {
			dst.transferFrom(src, 0, src.size());
			msg = "Backup Berhasil\nLokasi file backup berada di Kartu SD (bengkulu_wisata.bak)";
		} catch (IOException e) {
			e.printStackTrace();
			msg = "Backup Gagal";
		}
		AppHelper.showMessage(this, 1, msg);
	}

	private void importData() {
		final ChooserDialog chooserDialog = new ChooserDialog().with(this);
		chooserDialog.withFilter(false, false, "bak")
				.withStartFile(Environment.getExternalStorageDirectory().getPath());
		chooserDialog.withResources(R.string.pilih_file_backup, R.string.pilih, R.string.batal);
		chooserDialog.withChosenListener((path, file) -> {

			File target = new File(Environment.getDataDirectory(),
					"data/" + getPackageName() + "/databases/database.db");

			String msg;
			try (FileChannel src = new FileInputStream(file).getChannel();
					 FileChannel dst = new FileOutputStream(target).getChannel()) {
				dst.transferFrom(src, 0, src.size());
				msg = "Import Data Master Berhasil";
				populateData();
			} catch (IOException e) {
				e.printStackTrace();
				msg = "Import Data Master Gagal";
			}
			AppHelper.showMessage(MasterActivity.this, 1, msg);
		}).build().show();
	}
}
