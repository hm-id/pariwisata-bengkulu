package umb.teknik.pariwisata.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import umb.teknik.pariwisata.R;
import umb.teknik.pariwisata.dao.DaoTempatWisata;
import umb.teknik.pariwisata.helper.AppHelper;
import umb.teknik.pariwisata.model.TempatWisata;

public class InfoActivity extends AppCompatActivity {

	public static final String ID_WISATA = "idW";
	private DaoTempatWisata mDao;
	private ImageView mImage;
	private TextView mNama, mInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
		mImage = findViewById(R.id.info_image);
		mNama = findViewById(R.id.info_nama);
		mInfo = findViewById(R.id.info_keterangan);

		mDao = AppHelper.database(this).daoTempatWisata();

		long id = getIntent().getLongExtra(ID_WISATA, -1);
		if (id != -1) {
			setWisata(id);
		}
	}

	private void setWisata(long id) {
		TempatWisata wisata = mDao.get(id);
		if (wisata != null) {
			Glide.with(this).load(AppHelper.imageFile(this, wisata.nama)).into(mImage);
			mNama.setText(wisata.nama.toUpperCase());
			String info = "Alamat: " + wisata.alamat + "\n\n" + wisata.hariTersedia + "\n"
					+ wisata.jamTersedia + "\n\n" + wisata.keterangan;
			mInfo.setText(info);
		}
	}
}
